import { RootState } from '~/types'
import { MutationTree, ActionTree } from 'vuex'

export const state = (): RootState => ({
  data: [],
  filters: [],
})

export const mutations: MutationTree<RootState> = {
  setData(state: RootState, data): void {
    state.data = data
  },
  setFilters(state: RootState, data): void {
    state.filters = data
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit({ commit }, { app }) {
    const data = await app.$axios.$get('./data.json')
    const filters = await app.$axios.$get('./filters.json')
    commit('setFilters', filters)
    commit('setData', data)
  },
}
