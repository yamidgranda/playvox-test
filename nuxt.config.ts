export default {
  env: {},
  mode: 'universal',
  head: {
    title: 'Playvox test',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Nuxt.js TypeScript project',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#3B8070' },
  css: ['~/assets/styles/scss/base/base-styles.scss'],
  build: {},
  modules: ['@nuxtjs/style-resources', '@nuxtjs/axios'],
  styleResources: {
    scss: ['~/assets/styles/scss/abstracts/abstract-styles.scss'],
  },
  axios: {},
}
