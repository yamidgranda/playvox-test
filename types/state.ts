export interface RootState {
  data: Object
  filters: Object
}
