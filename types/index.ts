export * from './state'

export interface UiSelectOption {
  text: string // form text caption to client
  value: string // form input value to database
  icon?: string // icon name referece
  isSelected?: boolean // to add preselected options
  id?: string // optional unique ID (useful when different options has same value)
}

export interface Filter {
  value: string
  availableValues: UiSelectOption[]
}

export interface Person {
  id: number
  first_name: string
  last_name: string
  contact: Contact
  gender: string
  ip_address: string
  avatar: string
  address: Address
}

export interface Contact {
  email: string
  phone: string
}

export interface Address {
  city: string
  country: string
  postalCode: string
  state: string
  street: string
}

// -------------------------------------------------
// + ui elemets
// -------------------------------------------------

export interface UiSelectOption {
  text: string // form text caption to client
  value: string // form input value to database
  icon?: string // icon name referece
  isSelected?: boolean // to add preselected options
  id?: string // optional unique ID (useful when different options has same value)
}
